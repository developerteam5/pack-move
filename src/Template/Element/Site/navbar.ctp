<nav class="navbar navbar-inverse navbar_background">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <img src="<?= $this->Url->Build('/img/logo.png',true) ?>">
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
<!--       <ul class="nav navbar-nav">
        <li class="active"><a href="#">Home</a></li>
        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">Page 1 <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#">Page 1-1</a></li>
            <li><a href="#">Page 1-2</a></li>
            <li><a href="#">Page 1-3</a></li>
          </ul>
        </li>
        <li><a href="#">Page 2</a></li>
        <li><a href="#">Page 3</a></li>
      </ul> -->
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#">Home</a></li>
        <li><a href="#">About</a></li>
        <li><a href="#">Services</a></li>
        <li><a href="#">Branches</a></li>
        <li><a href="#">Contact</a></li>
      </ul>
    </div>
  </div>
</nav>
<div class="navbar2_background">
  <div class="container">
    <div class="row">
      <div class="col-sm-2 col-sm-25">
        <div class="col-sm-3 col-xs-1 fa_border"><center><i class="fas fa-map-pin"></i></center></div>
        <div class="col-sm-9 col-xs-5"><p>India</p><span>All India Service</span></div>
      </div>
      <div class="col-sm-2 col-sm-25">
        <div class="col-sm-3 col-xs-1 fa_border"><center><i class="fas fa-clock"></i></center></div>
        <div class="col-sm-9 col-xs-5"><p>Opening Hours</p><span>24/7 Available</span></div>
      </div>
      <div class="col-sm-2 col-sm-25">
        <div class="col-sm-3 col-xs-1 fa_border"><center><i class="fas fa-phone-alt"></i></center></div>
        <div class="col-sm-9 col-xs-5"><p>India</p><span>Call Us Now</span></div>
      </div>
      <div class="col-sm-2 col-sm-25">
        <div class="col-sm-3 col-xs-1 fa_border"><center><i class="fas fa-mail-bulk"></i></center></div>
        <div class="col-sm-9 col-xs-5"><p>9652045867</p><span>Email Us Now</span></div>
      </div>
      <div class="col-sm-2 col-sm-25 col-xs-12">
        <center>
         <button class="btn btn-primary" style="background-color: white; color:#0274be;padding: 14px 48px;     border-radius: 25px 25px 25px 25px;"><i class="fas fa-forward"></i> Get Quote</button>
        </center>
      </div>
     
    </div>
  </div>
</div>