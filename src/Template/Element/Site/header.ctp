<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="icon" href="<?= $this->Url->build('/img/favicon.png') ?>" sizes="32x32" />
  <?= $this->Html->css([
  	'bootstrap.min.css',
  	'site.css',
    'font-awesome/all.css'
  ]) ?>


  <link href="https://fonts.googleapis.com/css?family=Days+One&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
  
  <?= $this->Html->script([
  	'jquery.min.js',
  	'bootstrap.min.js'
  ]) ?>
</head>