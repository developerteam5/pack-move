<div class="container-fluid" style="padding-top: 32px;">
    <div class="row">
        <div class="col-12">
            
        </div>
        <div class="form-group col-sm-2 col-sm-25">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-arrow-circle-left"></i></span>
                <input class="form-control" id="from_places" placeholder="Moving from: address city or zip">
                <input id="origin" name="origin" required="" type="hidden">
            </div>
        </div>
       <div class="form-group col-sm-2 col-sm-25">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-arrow-circle-right"></i></span>
                <input class="form-control" id="to_places" placeholder="Moving To: address city or zip">
                <input id="destination" name="destination" required="" type="hidden">
            </div>
        </div>
        <div class="form-group col-sm-2 col-sm-25 col-xs-6">
            <!-- <label class="form-label">Move Size</label> -->
            <div class="input-group">

                <span class="input-group-addon"><i class="fas fa-home"></i></span>
                <select class="form-control" name="Total">
                    <option>1</option>
                </select>
            </div>
        </div>
        <div class="form-group col-sm-2 col-sm-25 col-xs-6">
            <!-- <label class="form-label">Move Date</label> -->
            <div class="input-group">
                <span class="input-group-addon"><i class="fas fa-calendar-check"></i></span>
                <input type="text" class="form-control" id="date_picker1">
            </div>
        </div>
        <script type="text/javascript">
            $('#date_picker1').datepicker({
                autoclose: true,
            });
        </script>
<!--         <div class="form-group col-sm-2 col-sm-25">
            <div class="input-group">
                <input type="checkbox" >Test1<br>
                <input type="checkbox" >Test1<br>
                <input type="checkbox" >Test1<br>
            </div>
        </div> -->
        <div class="form-group col-sm-2">
            <button class="btn btn-primary" style="padding: 14px 48px;     border-radius: 25px 25px 25px 25px;"><i class="fas fa-forward"></i> Get Quote</button>
        </div>
    </div>
</div>